Feature: Verify visa requirement for given countries
Scenario: visa requirement
Given I provide a nationality of "Japan"
And I select the reason "study"
And I state I am intending to stay for more than "6" months
When I submit the form
Then I will be informed "You�ll need a visa to study in the UK"

Scenario: second scenario
Given I select a nationality of "Japan"
And I provide the reason "tourism"
When I submit the form by click next
Then I will be informed that "I won�t need a visa to study in the UK"

Scenario: Third scenario
Given I provide a Nationality of "Russia"
And I select the reason for travel "tourism"
And I state I am not travelling or visiting a partner or family as "no"
When I submit the form by clicking next button
Then I will be informed as "I need a visa to come to the UK"
