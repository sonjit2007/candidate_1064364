package testRunner;

import java.io.IOException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import reUsableMethods.ReusableMethods;

public class Hook extends ReusableMethods{
	
	@Before
	public void load() throws IOException{
		initializeDriver();
		launchBaseURL("https://www.gov.uk/check-uk-visa/y");
	}
	@After
	public void closeDriver(){
		driver.close();
		driver.quit();
	}
}
