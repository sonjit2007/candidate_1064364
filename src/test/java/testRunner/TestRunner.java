package testRunner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;


@RunWith(Cucumber.class)
@CucumberOptions(features = "features/CheckVisaRequirement.feature", glue= "steps", 
								monochrome= true, dryRun=false, strict=false)

public class TestRunner {

}
