package restAssuredTest;

import static io.restassured.RestAssured.given;

import org.junit.BeforeClass;
import org.junit.Test;
import io.restassured.RestAssured;

public class ApiTestCases {
	
	@BeforeClass
	public static void setBaseURI() {
		RestAssured.baseURI = "https://www.gov.uk/check-uk-visa/y";	
	}	
	
	@Test
	public void getRequestForJapanWithStudy() {
		RestAssured.basePath = "/japan/study?response=longer_than_six_months&next=1";
		
		given()
		
		.when()
			.get()
		.then()
		.statusCode(200)
		.contentType("text/html");		
	}
	@Test
	public void getRequestForJapanWithTourism() {
		RestAssured.basePath = "/japan?response=tourism&next=1";
		
		given()
		
		.when()
			.get()
		.then()
		.statusCode(200)
		.contentType("text/html");		
	}
	@Test
	public void getRequestForRussiaWithTourism() {
		RestAssured.basePath = "/russia/tourism?response=no&next=1";
		
		given()
		
		.when()
			.get()
		.then()
		.statusCode(200)
		.contentType("text/html");
	}

}
