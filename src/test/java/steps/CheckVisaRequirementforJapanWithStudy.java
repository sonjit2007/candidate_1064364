package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageClasses.Check_Visa_Page;

public class CheckVisaRequirementforJapanWithStudy {
	
	public static Check_Visa_Page CheckVisaPage;
	
	@Given("^I provide a nationality of \"([^\"]*)\"$")
	public void i_provide_a_nationality_of(String nationality) throws Throwable {
		CheckVisaPage = new Check_Visa_Page();
		CheckVisaPage.selectCountryFromDropDown(nationality);
		CheckVisaPage.clickNextStepButton();
	}
	@And("^I select the reason \"([^\"]*)\"$")
	public void i_select_the_reason(String purposeOftheVisit) throws Throwable {
		CheckVisaPage.selectPurposeOftheVisist(purposeOftheVisit);
		CheckVisaPage.clickNextStepButton();
	}
	@And("^I state I am intending to stay for more than \"([^\"]*)\" months$")
	public void i_state_I_am_intending_to_stay_for_more_than_months(String lengthOfStay) throws Throwable {
		CheckVisaPage.selectLengthOftheStay(lengthOfStay);
	}
	@When("^I submit the form$")
	public void i_submit_the_form() throws Throwable {
		Thread.sleep(5000);
		CheckVisaPage.clickNextStepButton();
	}
	@Then("^I will be informed \"([^\"]*)\"$")
	public void i_will_be_informed(String visarequirement) throws Throwable {
		Thread.sleep(2000);		
		CheckVisaPage.verifyVisaRequirementStatus("You�ll need a visa to study in the UK");
	}
}
