package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageClasses.Check_Visa_Page;

public class CheckVisaRequirementforRussiaWithTourism {
	public static Check_Visa_Page CheckVisaPage;
	
	@Given("^I provide a Nationality of \"([^\"]*)\"$")
	public void i_provide_a_Nationality_of(String nationality) throws Throwable {
		CheckVisaPage = new Check_Visa_Page();
		CheckVisaPage.selectCountryFromDropDown(nationality);
		CheckVisaPage.clickNextStepButton();
	}

	@And("^I select the reason for travel \"([^\"]*)\"$")
	public void i_select_the_reason_for_travel(String purposeOftheVisit) throws Throwable {
		CheckVisaPage.selectPurposeOftheVisist(purposeOftheVisit);
		CheckVisaPage.clickNextStepButton();
	}

	@Given("^I state I am not travelling or visiting a partner or family as \"([^\"]*)\"$")
	public void i_state_I_am_not_travelling_or_visiting_a_partner_or_family_as(String data) throws Throwable {
		CheckVisaPage.selectWhetherYouareVisitingFamily(data);
	}

	@When("^I submit the form by clicking next button$")
	public void i_submit_the_form_by_clicking_next_button() throws Throwable {
		CheckVisaPage.clickNextStepButton();
	}
	@Then("^I will be informed as \"([^\"]*)\"$")
	public void i_will_be_informed_as(String arg1) throws Throwable {
		CheckVisaPage.verifyVisaRequirementStatus("You�ll need a visa to come to the UK");
	}

}
