package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageClasses.Check_Visa_Page;

public class CheckVisaRequirementforJapanWithTourism {
	public static Check_Visa_Page CheckVisaPage;
	@Given("^I select a nationality of \"([^\"]*)\"$")
	public void i_select_a_nationality_of(String nationality) throws Throwable {
		CheckVisaPage = new Check_Visa_Page();
		CheckVisaPage.selectCountryFromDropDown(nationality);
		CheckVisaPage.clickNextStepButton();
	}

	@And("^I provide the reason \"([^\"]*)\"$")
	public void i_provide_the_reason(String purposeOftheVisit) throws Throwable {
		CheckVisaPage.selectPurposeOftheVisist(purposeOftheVisit);
	}

	@When("^I submit the form by click next$")
	public void i_submit_the_form_by_click_next() throws Throwable {
		CheckVisaPage.clickNextStepButton();
	}

	@Then("^I will be informed that \"([^\"]*)\"$")
	public void i_will_be_informed_that(String arg1) throws Throwable {
		CheckVisaPage.verifyVisaRequirementStatus("You won�t need a visa to come to the UK");
	}

}
