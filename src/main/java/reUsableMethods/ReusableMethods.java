package reUsableMethods;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class ReusableMethods {
	
	public static WebDriver driver;
		
	public void initializeDriver(){
		System.setProperty("webdriver.chrome.driver", "./lib/chromedriver_.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	public void launchBaseURL(String baseURL){
		driver.get(baseURL);
	}	
	public void verifyTitle(String Title) throws Throwable{
		String title = driver.getTitle();
		assertEquals(title.trim(),Title, "User in Different Page");	
	}
	public void enterById(String locator, String data){
		driver.findElement(By.id(locator)).sendKeys(data);
	}
	public void clickByXpath(String locator){
		driver.findElement(By.xpath(locator)).click();
	}
	public void verifyById(String locator, String data) throws Throwable{
		//String text = driver.findElement(By.id(locator)).getText();		
	}
	public void selectDropDownValue(String locator, String data){
		WebElement dropDown = driver.findElement(By.id(locator));
		Select drop = new Select(dropDown);
		drop.selectByVisibleText(data);
	}
	public void checkRadioButton(String locator, String data){
		driver.findElement(By.xpath(locator)).click();
	}
	public void verifyText(String locator, String data) throws Throwable{
		String status = driver.findElement(By.xpath(locator)).getText();
		assertEquals(status.trim(),data, "Visa requirement Status messages are not matching");	
	}
}
