package pageClasses;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;

import reUsableMethods.ReusableMethods;

public class Check_Visa_Page extends ReusableMethods{
	
	public Check_Visa_Page verifyPage() throws Throwable{
		verifyTitle("Check if you need a UK visa - GOV.UK");
		return this;
	}
	public Check_Visa_Page selectCountryFromDropDown(String data){
		selectDropDownValue("response", data);
		return this;
	}
	public Check_Visa_Page clickNextStepButton(){
		clickByXpath("//div[@id='current-question']/button");
		return this;
	}
	public Check_Visa_Page selectPurposeOftheVisist(String data){
		checkRadioButton(("//input[@value = 'userInput']").replace("userInput", data), data);
		return this;
	}
	public Check_Visa_Page selectLengthOftheStay(String data){
		checkRadioButton(("//input[@value='longer_than_six_months']"), data);
		return this;
	}
	public Check_Visa_Page verifyVisaRequirementStatus(String data) throws Throwable{
		String status = driver.findElement(By.xpath("//div[@id='result-info']/div[2]/h2")).getText();
		assertEquals(status.trim(),data);
		return this;
	}
	public Check_Visa_Page selectWhetherYouareVisitingFamily(String data){
		checkRadioButton(("//input[@value = 'userInput']").replace("userInput", data), data);
		return this;
	}
}
